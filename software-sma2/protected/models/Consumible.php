<?php

/**
 * This is the model class for table "Consumible".
 *
 * The followings are the available columns in table 'Consumible':
 * @property integer $id
 * @property string $descripcion
 * @property string $fechaIngreso
 * @property string $fila
 * @property string $columna
 * @property string $estadoUbicacion
 * @property string $Estanteria_codigo
 * @property integer $Presentacion_id
 * @property integer $UnidadMedida_id
 *
 * The followings are the available model relations:
 * @property Estanteria $estanteriaCodigo
 * @property Presentacion $presentacion
 * @property Unidadmedida $unidadMedida
 */
class Consumible extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Consumible';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, descripcion, fechaIngreso, Estanteria_codigo, Presentacion_id, UnidadMedida_id', 'required'),
			array('id, Presentacion_id, UnidadMedida_id', 'numerical', 'integerOnly'=>true),
			array('descripcion', 'length', 'max'=>45),
			array('fila, columna', 'length', 'max'=>5),
			array('estadoUbicacion, Estanteria_codigo', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, descripcion, fechaIngreso, fila, columna, estadoUbicacion, Estanteria_codigo, Presentacion_id, UnidadMedida_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'estanteriaCodigo' => array(self::BELONGS_TO, 'Estanteria', 'Estanteria_codigo'),
			'presentacion' => array(self::BELONGS_TO, 'Presentacion', 'Presentacion_id'),
			'unidadMedida' => array(self::BELONGS_TO, 'Unidadmedida', 'UnidadMedida_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'descripcion' => 'Descripcion',
			'fechaIngreso' => 'Fecha Ingreso',
			'fila' => 'Fila',
			'columna' => 'Columna',
			'estadoUbicacion' => 'Estado Ubicacion',
			'Estanteria_codigo' => 'Estanteria Codigo',
			'Presentacion_id' => 'Presentacion',
			'UnidadMedida_id' => 'Unidad Medida',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('fechaIngreso',$this->fechaIngreso,true);
		$criteria->compare('fila',$this->fila,true);
		$criteria->compare('columna',$this->columna,true);
		$criteria->compare('estadoUbicacion',$this->estadoUbicacion,true);
		$criteria->compare('Estanteria_codigo',$this->Estanteria_codigo,true);
		$criteria->compare('Presentacion_id',$this->Presentacion_id);
		$criteria->compare('UnidadMedida_id',$this->UnidadMedida_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Consumible the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
