<?php

/**
 * This is the model class for table "Estanteria".
 *
 * The followings are the available columns in table 'Estanteria':
 * @property string $codigo
 * @property string $decripcion
 * @property integer $cantFila
 * @property integer $cantColumna
 * @property string $imagen
 * @property integer $AreaAlmacen_id
 *
 * The followings are the available model relations:
 * @property Consumible[] $consumibles
 * @property Areaalmacen $areaAlmacen
 * @property Herramiencomun[] $herramiencomuns
 * @property Herramientaespecial[] $herramientaespecials
 * @property Rotable[] $rotables
 */
class Estanteria extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Estanteria';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('codigo, decripcion, AreaAlmacen_id', 'required'),
			array('cantFila, cantColumna, AreaAlmacen_id', 'numerical', 'integerOnly'=>true),
			array('codigo', 'length', 'max'=>10),
			array('decripcion', 'length', 'max'=>45),
			array('imagen', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('codigo, decripcion, cantFila, cantColumna, imagen, AreaAlmacen_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'consumibles' => array(self::HAS_MANY, 'Consumible', 'Estanteria_codigo'),
			'areaAlmacen' => array(self::BELONGS_TO, 'Areaalmacen', 'AreaAlmacen_id'),
			'herramiencomuns' => array(self::HAS_MANY, 'Herramiencomun', 'Estanteria_codigo'),
			'herramientaespecials' => array(self::HAS_MANY, 'Herramientaespecial', 'estanteria_codigo'),
			'rotables' => array(self::HAS_MANY, 'Rotable', 'Estanteria_codigo'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'codigo' => 'Codigo',
			'decripcion' => 'Decripcion',
			'cantFila' => 'Cant Fila',
			'cantColumna' => 'Cant Columna',
			'imagen' => 'Imagen',
			'AreaAlmacen_id' => 'Area Almacen',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('codigo',$this->codigo,true);
		$criteria->compare('decripcion',$this->decripcion,true);
		$criteria->compare('cantFila',$this->cantFila);
		$criteria->compare('cantColumna',$this->cantColumna);
		$criteria->compare('imagen',$this->imagen,true);
		$criteria->compare('AreaAlmacen_id',$this->AreaAlmacen_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Estanteria the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
