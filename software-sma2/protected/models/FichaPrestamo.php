<?php

/**
 * This is the model class for table "FichaPrestamo".
 *
 * The followings are the available columns in table 'FichaPrestamo':
 * @property integer $id
 * @property string $fechaPrestamo
 * @property string $fechaDevolucion
 * @property string $observacion
 * @property integer $HerramientaEspecial_codigo
 * @property integer $cantidad
 * @property integer $Tecnico_ci
 * @property string $PersonalOficial_codigo
 */
class FichaPrestamo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'FichaPrestamo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, fechaPrestamo, HerramientaEspecial_codigo, Tecnico_ci, PersonalOficial_codigo', 'required'),
			array('id, HerramientaEspecial_codigo, cantidad, Tecnico_ci', 'numerical', 'integerOnly'=>true),
			array('observacion', 'length', 'max'=>45),
			array('PersonalOficial_codigo', 'length', 'max'=>20),
			array('fechaDevolucion', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, fechaPrestamo, fechaDevolucion, observacion, HerramientaEspecial_codigo, cantidad, Tecnico_ci, PersonalOficial_codigo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'fechaPrestamo' => 'Fecha Prestamo',
			'fechaDevolucion' => 'Fecha Devolucion',
			'observacion' => 'Observacion',
			'HerramientaEspecial_codigo' => 'Herramienta Especial Codigo',
			'cantidad' => 'Cantidad',
			'Tecnico_ci' => 'Tecnico Ci',
			'PersonalOficial_codigo' => 'Personal Oficial Codigo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('fechaPrestamo',$this->fechaPrestamo,true);
		$criteria->compare('fechaDevolucion',$this->fechaDevolucion,true);
		$criteria->compare('observacion',$this->observacion,true);
		$criteria->compare('HerramientaEspecial_codigo',$this->HerramientaEspecial_codigo);
		$criteria->compare('cantidad',$this->cantidad);
		$criteria->compare('Tecnico_ci',$this->Tecnico_ci);
		$criteria->compare('PersonalOficial_codigo',$this->PersonalOficial_codigo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FichaPrestamo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
