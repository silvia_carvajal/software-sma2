<?php

/**
 * This is the model class for table "HerramientaEspecial".
 *
 * The followings are the available columns in table 'HerramientaEspecial':
 * @property integer $id
 * @property string $descripcion
 * @property string $imagen
 * @property string $nroCertificado
 * @property string $nroFactura
 * @property string $nroParte
 * @property string $nroSerie
 * @property string $fechaCalibracion
 * @property string $eligilidad
 * @property string $detalleOrganizacion
 * @property string $autoridadAprovacion
 * @property string $fechaEntrega
 * @property string $fechaIngreso
 * @property string $contactoOrganizacon
 * @property string $fabricante
 * @property string $agenciaCertificacion
 * @property string $condicion
 * @property string $commingInspection
 * @property string $fila
 * @property string $columna
 * @property string $estadoUbicacion
 * @property string $estanteria_codigo
 * @property integer $estado_id
 * @property integer $TipoItem_id
 */
class HerramientaEspecial extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'HerramientaEspecial';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, descripcion, nroCertificado, nroFactura, nroParte, nroSerie, estanteria_codigo, estado_id, TipoItem_id', 'required'),
			array('id, estado_id, TipoItem_id', 'numerical', 'integerOnly'=>true),
			array('descripcion, fabricante, condicion', 'length', 'max'=>50),
			array('nroCertificado, nroFactura, nroParte, nroSerie', 'length', 'max'=>30),
			array('eligilidad', 'length', 'max'=>20),
			array('detalleOrganizacion, autoridadAprovacion, contactoOrganizacon, agenciaCertificacion', 'length', 'max'=>100),
			array('commingInspection', 'length', 'max'=>45),
			array('fila, columna', 'length', 'max'=>5),
			array('estadoUbicacion, estanteria_codigo', 'length', 'max'=>10),
			array('imagen, fechaCalibracion, fechaEntrega, fechaIngreso', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, descripcion, imagen, nroCertificado, nroFactura, nroParte, nroSerie, fechaCalibracion, eligilidad, detalleOrganizacion, autoridadAprovacion, fechaEntrega, fechaIngreso, contactoOrganizacon, fabricante, agenciaCertificacion, condicion, commingInspection, fila, columna, estadoUbicacion, estanteria_codigo, estado_id, TipoItem_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'descripcion' => 'Descripcion',
			'imagen' => 'Imagen',
			'nroCertificado' => 'Nro Certificado',
			'nroFactura' => 'Nro Factura',
			'nroParte' => 'Nro Parte',
			'nroSerie' => 'Nro Serie',
			'fechaCalibracion' => 'Fecha Calibracion',
			'eligilidad' => 'Eligilidad',
			'detalleOrganizacion' => 'Detalle Organizacion',
			'autoridadAprovacion' => 'Autoridad Aprovacion',
			'fechaEntrega' => 'Fecha Entrega',
			'fechaIngreso' => 'Fecha Ingreso',
			'contactoOrganizacon' => 'Contacto Organizacon',
			'fabricante' => 'Fabricante',
			'agenciaCertificacion' => 'Agencia Certificacion',
			'condicion' => 'Condicion',
			'commingInspection' => 'Comming Inspection',
			'fila' => 'Fila',
			'columna' => 'Columna',
			'estadoUbicacion' => 'Estado Ubicacion',
			'estanteria_codigo' => 'Estanteria Codigo',
			'estado_id' => 'Estado',
			'TipoItem_id' => 'Tipo Item',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('imagen',$this->imagen,true);
		$criteria->compare('nroCertificado',$this->nroCertificado,true);
		$criteria->compare('nroFactura',$this->nroFactura,true);
		$criteria->compare('nroParte',$this->nroParte,true);
		$criteria->compare('nroSerie',$this->nroSerie,true);
		$criteria->compare('fechaCalibracion',$this->fechaCalibracion,true);
		$criteria->compare('eligilidad',$this->eligilidad,true);
		$criteria->compare('detalleOrganizacion',$this->detalleOrganizacion,true);
		$criteria->compare('autoridadAprovacion',$this->autoridadAprovacion,true);
		$criteria->compare('fechaEntrega',$this->fechaEntrega,true);
		$criteria->compare('fechaIngreso',$this->fechaIngreso,true);
		$criteria->compare('contactoOrganizacon',$this->contactoOrganizacon,true);
		$criteria->compare('fabricante',$this->fabricante,true);
		$criteria->compare('agenciaCertificacion',$this->agenciaCertificacion,true);
		$criteria->compare('condicion',$this->condicion,true);
		$criteria->compare('commingInspection',$this->commingInspection,true);
		$criteria->compare('fila',$this->fila,true);
		$criteria->compare('columna',$this->columna,true);
		$criteria->compare('estadoUbicacion',$this->estadoUbicacion,true);
		$criteria->compare('estanteria_codigo',$this->estanteria_codigo,true);
		$criteria->compare('estado_id',$this->estado_id);
		$criteria->compare('TipoItem_id',$this->TipoItem_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return HerramientaEspecial the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
