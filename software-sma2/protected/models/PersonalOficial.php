<?php

/**
 * This is the model class for table "PersonalOficial".
 *
 * The followings are the available columns in table 'PersonalOficial':
 * @property string $codigo
 * @property integer $ci
 * @property string $nombres
 * @property string $apPat
 * @property string $apMat
 * @property string $sexo
 * @property string $fechaNac
 * @property string $direccion
 * @property string $tipoSangre
 * @property integer $Cargo_id
 * @property integer $Grado_id
 */
class PersonalOficial extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'PersonalOficial';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('codigo, ci, nombres, apPat, apMat, sexo, fechaNac, direccion, tipoSangre, Cargo_id, Grado_id', 'required'),
			array('ci, Cargo_id, Grado_id', 'numerical', 'integerOnly'=>true),
			array('codigo, apPat, apMat', 'length', 'max'=>20),
			array('nombres, direccion', 'length', 'max'=>45),
			array('sexo', 'length', 'max'=>1),
			array('tipoSangre', 'length', 'max'=>15),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('codigo, ci, nombres, apPat, apMat, sexo, fechaNac, direccion, tipoSangre, Cargo_id, Grado_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'codigo' => 'Codigo',
			'ci' => 'Ci',
			'nombres' => 'Nombres',
			'apPat' => 'Ap Pat',
			'apMat' => 'Ap Mat',
			'sexo' => 'Sexo',
			'fechaNac' => 'Fecha Nac',
			'direccion' => 'Direccion',
			'tipoSangre' => 'Tipo Sangre',
			'Cargo_id' => 'Cargo',
			'Grado_id' => 'Grado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('codigo',$this->codigo,true);
		$criteria->compare('ci',$this->ci);
		$criteria->compare('nombres',$this->nombres,true);
		$criteria->compare('apPat',$this->apPat,true);
		$criteria->compare('apMat',$this->apMat,true);
		$criteria->compare('sexo',$this->sexo,true);
		$criteria->compare('fechaNac',$this->fechaNac,true);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('tipoSangre',$this->tipoSangre,true);
		$criteria->compare('Cargo_id',$this->Cargo_id);
		$criteria->compare('Grado_id',$this->Grado_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PersonalOficial the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
