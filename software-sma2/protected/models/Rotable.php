<?php

/**
 * This is the model class for table "Rotable".
 *
 * The followings are the available columns in table 'Rotable':
 * @property integer $id
 * @property string $descripcion
 * @property string $imagen
 * @property string $fechaIngreso
 * @property string $nroFactura
 * @property string $nroCertifcado
 * @property string $nroParte
 * @property string $nroSerie
 * @property string $condicion
 * @property string $fabricante
 * @property string $fechaEntrega
 * @property string $eligibilidad
 * @property string $detalleOrganizacion
 * @property string $contactoOrganizacion
 * @property string $fila
 * @property string $columna
 * @property string $estadoUbicacion
 * @property string $Estanteria_codigo
 * @property integer $estado_id
 * @property integer $TipoAvion_id
 * @property integer $TipoItem_id
 *
 * The followings are the available model relations:
 * @property Estanteria $estanteriaCodigo
 * @property Estado $estado
 * @property Tipoavion $tipoAvion
 * @property Tipoitem $tipoItem
 * @property Salidarotable[] $salidarotables
 */
class Rotable extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Rotable';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, descripcion, nroFactura, nroCertifcado, nroParte, nroSerie, condicion, fabricante, eligibilidad, detalleOrganizacion, contactoOrganizacion, Estanteria_codigo, estado_id, TipoAvion_id, TipoItem_id', 'required'),
			array('id, estado_id, TipoAvion_id, TipoItem_id', 'numerical', 'integerOnly'=>true),
			array('descripcion, nroFactura, nroCertifcado, nroParte, nroSerie, condicion, fabricante, eligibilidad', 'length', 'max'=>50),
			array('detalleOrganizacion, contactoOrganizacion', 'length', 'max'=>100),
			array('fila, columna', 'length', 'max'=>5),
			array('estadoUbicacion, Estanteria_codigo', 'length', 'max'=>10),
			array('imagen, fechaIngreso, fechaEntrega', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, descripcion, imagen, fechaIngreso, nroFactura, nroCertifcado, nroParte, nroSerie, condicion, fabricante, fechaEntrega, eligibilidad, detalleOrganizacion, contactoOrganizacion, fila, columna, estadoUbicacion, Estanteria_codigo, estado_id, TipoAvion_id, TipoItem_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'estanteriaCodigo' => array(self::BELONGS_TO, 'Estanteria', 'Estanteria_codigo'),
			'estado' => array(self::BELONGS_TO, 'Estado', 'estado_id'),
			'tipoAvion' => array(self::BELONGS_TO, 'Tipoavion', 'TipoAvion_id'),
			'tipoItem' => array(self::BELONGS_TO, 'Tipoitem', 'TipoItem_id'),
			'salidarotables' => array(self::HAS_MANY, 'Salidarotable', 'Rotable_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'descripcion' => 'Descripcion',
			'imagen' => 'Imagen',
			'fechaIngreso' => 'Fecha Ingreso',
			'nroFactura' => 'Nro Factura',
			'nroCertifcado' => 'Nro Certifcado',
			'nroParte' => 'Nro Parte',
			'nroSerie' => 'Nro Serie',
			'condicion' => 'Condicion',
			'fabricante' => 'Fabricante',
			'fechaEntrega' => 'Fecha Entrega',
			'eligibilidad' => 'Eligibilidad',
			'detalleOrganizacion' => 'Detalle Organizacion',
			'contactoOrganizacion' => 'Contacto Organizacion',
			'fila' => 'Fila',
			'columna' => 'Columna',
			'estadoUbicacion' => 'Estado Ubicacion',
			'Estanteria_codigo' => 'Estanteria Codigo',
			'estado_id' => 'Estado',
			'TipoAvion_id' => 'Tipo Avion',
			'TipoItem_id' => 'Tipo Item',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('imagen',$this->imagen,true);
		$criteria->compare('fechaIngreso',$this->fechaIngreso,true);
		$criteria->compare('nroFactura',$this->nroFactura,true);
		$criteria->compare('nroCertifcado',$this->nroCertifcado,true);
		$criteria->compare('nroParte',$this->nroParte,true);
		$criteria->compare('nroSerie',$this->nroSerie,true);
		$criteria->compare('condicion',$this->condicion,true);
		$criteria->compare('fabricante',$this->fabricante,true);
		$criteria->compare('fechaEntrega',$this->fechaEntrega,true);
		$criteria->compare('eligibilidad',$this->eligibilidad,true);
		$criteria->compare('detalleOrganizacion',$this->detalleOrganizacion,true);
		$criteria->compare('contactoOrganizacion',$this->contactoOrganizacion,true);
		$criteria->compare('fila',$this->fila,true);
		$criteria->compare('columna',$this->columna,true);
		$criteria->compare('estadoUbicacion',$this->estadoUbicacion,true);
		$criteria->compare('Estanteria_codigo',$this->Estanteria_codigo,true);
		$criteria->compare('estado_id',$this->estado_id);
		$criteria->compare('TipoAvion_id',$this->TipoAvion_id);
		$criteria->compare('TipoItem_id',$this->TipoItem_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Rotable the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
