<?php

/**
 * This is the model class for table "SalidaRotable".
 *
 * The followings are the available columns in table 'SalidaRotable':
 * @property integer $id
 * @property string $fechaSalida
 * @property integer $Rotable_id
 * @property integer $Tecnico_id
 * @property string $PersonalOficial_codigo
 */
class SalidaRotable extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'SalidaRotable';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, fechaSalida, Rotable_id, Tecnico_id, PersonalOficial_codigo', 'required'),
			array('id, Rotable_id, Tecnico_id', 'numerical', 'integerOnly'=>true),
			array('PersonalOficial_codigo', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, fechaSalida, Rotable_id, Tecnico_id, PersonalOficial_codigo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'fechaSalida' => 'Fecha Salida',
			'Rotable_id' => 'Rotable',
			'Tecnico_id' => 'Tecnico',
			'PersonalOficial_codigo' => 'Personal Oficial Codigo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('fechaSalida',$this->fechaSalida,true);
		$criteria->compare('Rotable_id',$this->Rotable_id);
		$criteria->compare('Tecnico_id',$this->Tecnico_id);
		$criteria->compare('PersonalOficial_codigo',$this->PersonalOficial_codigo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SalidaRotable the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
