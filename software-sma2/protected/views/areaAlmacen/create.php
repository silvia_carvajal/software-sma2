<?php
/* @var $this AreaAlmacenController */
/* @var $model AreaAlmacen */

$this->breadcrumbs=array(
	'Area Almacens'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Listar AreaAlmacen', 'url'=>array('index')),
	array('label'=>'Administrar AreaAlmacen', 'url'=>array('admin')),
);
?>

<h1>Nuevo AreaAlmacen</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>