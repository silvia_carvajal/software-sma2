<?php
/* @var $this AreaAlmacenController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Area Almacens',
);

$this->menu=array(
	array('label'=>'Nuevo AreaAlmacen', 'url'=>array('create')),
	array('label'=>'Administrar AreaAlmacen', 'url'=>array('admin')),
);
?>

<h1>Area Almacens</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
