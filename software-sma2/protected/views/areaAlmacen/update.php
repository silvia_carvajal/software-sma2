<?php
/* @var $this AreaAlmacenController */
/* @var $model AreaAlmacen */

$this->breadcrumbs=array(
	'Area Almacens'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Editar',
);

$this->menu=array(
	array('label'=>'Listar AreaAlmacen', 'url'=>array('index')),
	array('label'=>'Nuevo AreaAlmacen', 'url'=>array('create')),
	array('label'=>'Ver AreaAlmacen', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar AreaAlmacen', 'url'=>array('admin')),
);
?>

<h1>Editar AreaAlmacen <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>