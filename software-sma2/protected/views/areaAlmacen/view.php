<?php
/* @var $this AreaAlmacenController */
/* @var $model AreaAlmacen */

$this->breadcrumbs=array(
	'Area Almacens'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Listar AreaAlmacen', 'url'=>array('index')),
	array('label'=>'Nuevo AreaAlmacen', 'url'=>array('create')),
	array('label'=>'Editar AreaAlmacen', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Elimininar AreaAlmacen', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar AreaAlmacen', 'url'=>array('admin')),
);
?>

<h1>Ver AreaAlmacen #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'descripcion',
	),
)); ?>
