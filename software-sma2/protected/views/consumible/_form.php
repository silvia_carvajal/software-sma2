<?php
/* @var $this ConsumibleController */
/* @var $model Consumible */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'consumible-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
		<?php echo $form->error($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
		<?php echo $form->textField($model,'descripcion',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fechaIngreso'); ?>
		<?php echo $form->textField($model,'fechaIngreso'); ?>
		<?php echo $form->error($model,'fechaIngreso'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fila'); ?>
		<?php echo $form->textField($model,'fila',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'fila'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'columna'); ?>
		<?php echo $form->textField($model,'columna',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'columna'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estadoUbicacion'); ?>
		<?php echo $form->textField($model,'estadoUbicacion',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'estadoUbicacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Estanteria_codigo'); ?>
		<?php echo $form->dropDownList($model,'Estanteria_codigo',Chtml::listData(Estanteria::model()->findAll(),'codigo','descripcion'), array('null'=>'Ninguno..')); ?>
		<?php echo $form->error($model,'Estanteria_codigo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Presentacion_id'); ?>
		<?php echo $form->dropDownList($model,'Presentacion_id',Chtml::listData(Presentacion::model()->findAll(),'id','descripcion'), array('null'=>'Ninguno..')); ?>
		<?php echo $form->error($model,'Presentacion_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'UnidadMedida_id'); ?>
		<?php echo $form->dropDownList($model,'UnidadMedida_id',Chtml::listData(UnidadMedida::model()->findAll(),'id','descripcion'), array('null'=>'Ninguno..')); ?>
		<?php echo $form->error($model,'UnidadMedida_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->