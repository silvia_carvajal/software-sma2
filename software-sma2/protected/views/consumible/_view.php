<?php
/* @var $this ConsumibleController */
/* @var $data Consumible */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaIngreso')); ?>:</b>
	<?php echo CHtml::encode($data->fechaIngreso); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fila')); ?>:</b>
	<?php echo CHtml::encode($data->fila); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('columna')); ?>:</b>
	<?php echo CHtml::encode($data->columna); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estadoUbicacion')); ?>:</b>
	<?php echo CHtml::encode($data->estadoUbicacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Estanteria_codigo')); ?>:</b>
	<?php echo CHtml::encode($data->Estanteria_codigo); ?>
	<br />

	
	<b><?php echo CHtml::encode($data->getAttributeLabel('Presentacion_id')); ?>:</b>
	<?php echo CHtml::encode($data->Presentacion_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('UnidadMedida_id')); ?>:</b>
	<?php echo CHtml::encode($data->UnidadMedida_id); ?>
	<br />



</div>