<?php
/* @var $this ConsumibleController */
/* @var $model Consumible */

$this->breadcrumbs=array(
	'Consumibles'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar Consumible', 'url'=>array('index')),
	array('label'=>'Nuevo Consumible', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#consumible-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Consumibles</h1>

<p>
Opcionalmente, puede introducir un operador de comparación (<, <=,>,> =, <> o =) al principio de cada uno de los valores de búsqueda para especificar cómo se debe hacer la comparación.
</p>


<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'consumible-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'descripcion',
		'fechaIngreso',
		'fila',
		'columna',
		'estadoUbicacion',
		
		'Estanteria_codigo',
		'Presentacion_id',
		'UnidadMedida_id',
		
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
