<?php
/* @var $this ConsumibleController */
/* @var $model Consumible */

$this->breadcrumbs=array(
	'Consumibles'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Listar Consumible', 'url'=>array('index')),
	array('label'=>'Administrar Consumible', 'url'=>array('admin')),
);
?>

<h1>Create Consumible</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>