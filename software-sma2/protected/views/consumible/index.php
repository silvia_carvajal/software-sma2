<?php
/* @var $this ConsumibleController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Consumibles',
);

$this->menu=array(
	array('label'=>'Nuevo Consumible', 'url'=>array('create')),
	array('label'=>'Administrar Consumible', 'url'=>array('admin')),
);
?>

<h1>Consumibles</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
