<?php
/* @var $this ConsumibleController */
/* @var $model Consumible */

$this->breadcrumbs=array(
	'Consumibles'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Editar',
);

$this->menu=array(
	array('label'=>'Listar Consumible', 'url'=>array('index')),
	array('label'=>'Nuevo Consumible', 'url'=>array('create')),
	array('label'=>'Ver Consumible', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrador Consumible', 'url'=>array('admin')),
);
?>

<h1>Update Consumible <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>