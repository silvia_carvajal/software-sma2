<?php
/* @var $this ConsumibleController */
/* @var $model Consumible */

$this->breadcrumbs=array(
	'Consumibles'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Listar Consumible', 'url'=>array('index')),
	array('label'=>'Nuevo Consumible', 'url'=>array('create')),
	array('label'=>'Editar Consumible', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Eliminar Consumible', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Consumible', 'url'=>array('admin')),
);
?>

<h1>View Consumible #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'descripcion',
		'fechaIngreso',
		'fila',
		'columna',
		'estadoUbicacion',
		'Estanteria_codigo',
		'Presentacion_id',
		'UnidadMedida_id',
	),
)); ?>
