<?php
/* @var $this ContactoTecnicoController */
/* @var $model ContactoTecnico */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contacto-tecnico-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
		<?php echo $form->error($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'descripcionContacto'); ?>
		<?php echo $form->textField($model,'descripcionContacto',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'descripcionContacto'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Tecnico_ci'); ?>
		<?php echo $form->textField($model,'Tecnico_ci'); ?>
		<?php echo $form->error($model,'Tecnico_ci'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TipoContacto_id'); ?>
		<?php echo $form->dropDownList($model,'TipoContacto_id',Chtml::listData(TipoContacto::model()->findAll(),'id','descripcion'), array('null'=>'Ninguno..')); ?>
		<?php echo $form->error($model,'TipoContacto_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->