<?php
/* @var $this ContactoTecnicoController */
/* @var $model ContactoTecnico */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'descripcionContacto'); ?>
		<?php echo $form->textField($model,'descripcionContacto',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Tecnico_ci'); ?>
		<?php echo $form->textField($model,'Tecnico_ci'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TipoContacto_id'); ?>
		<?php echo $form->textField($model,'TipoContacto_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->