<?php
/* @var $this ContactoTecnicoController */
/* @var $data ContactoTecnico */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcionContacto')); ?>:</b>
	<?php echo CHtml::encode($data->descripcionContacto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Tecnico_ci')); ?>:</b>
	<?php echo CHtml::encode($data->Tecnico_ci); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TipoContacto_id')); ?>:</b>
	<?php echo CHtml::encode($data->TipoContacto_id); ?>
	<br />


</div>