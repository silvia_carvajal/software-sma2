<?php
/* @var $this ContactoTecnicoController */
/* @var $model ContactoTecnico */

$this->breadcrumbs=array(
	'Contacto Tecnicos'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Listar ContactoTecnico', 'url'=>array('index')),
	array('label'=>'Administrar ContactoTecnico', 'url'=>array('admin')),
);
?>

<h1>Create ContactoTecnico</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>