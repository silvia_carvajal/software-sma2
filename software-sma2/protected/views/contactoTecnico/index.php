<?php
/* @var $this ContactoTecnicoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Contacto Tecnicos',
);

$this->menu=array(
	array('label'=>'Nuevo ContactoTecnico', 'url'=>array('create')),
	array('label'=>'Administrar ContactoTecnico', 'url'=>array('admin')),
);
?>

<h1>Contacto Tecnicos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
