<?php
/* @var $this ContactoTecnicoController */
/* @var $model ContactoTecnico */

$this->breadcrumbs=array(
	'Contacto Tecnicos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Editar',
);

$this->menu=array(
	array('label'=>'Listar ContactoTecnico', 'url'=>array('index')),
	array('label'=>'Editar ContactoTecnico', 'url'=>array('create')),
	array('label'=>'Ver ContactoTecnico', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar ContactoTecnico', 'url'=>array('admin')),
);
?>

<h1>Update ContactoTecnico <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>