<?php
/* @var $this ContactoTecnicoController */
/* @var $model ContactoTecnico */

$this->breadcrumbs=array(
	'Contacto Tecnicos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Listar ContactoTecnico', 'url'=>array('index')),
	array('label'=>'Nuevo ContactoTecnico', 'url'=>array('create')),
	array('label'=>'Editar ContactoTecnico', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Eliminar ContactoTecnico', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Aministrar ContactoTecnico', 'url'=>array('admin')),
);
?>

<h1>View ContactoTecnico #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'descripcionContacto',
		'Tecnico_ci',
		'TipoContacto_id',
	),
)); ?>
