<?php
/* @var $this EstadoController */
/* @var $model Estado */

$this->breadcrumbs=array(
	'Estados'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Listar Estado', 'url'=>array('index')),
	array('label'=>'Administrar Estado', 'url'=>array('admin')),
);
?>

<h1>Nuevo Estado</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>