<?php
/* @var $this EstanteriaController */
/* @var $model Estanteria */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'estanteria-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'codigo'); ?>
		<?php echo $form->textField($model,'codigo',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'codigo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'decripcion'); ?>
		<?php echo $form->textField($model,'decripcion',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'decripcion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cantFila'); ?>
		<?php echo $form->textField($model,'cantFila'); ?>
		<?php echo $form->error($model,'cantFila'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cantColumna'); ?>
		<?php echo $form->textField($model,'cantColumna'); ?>
		<?php echo $form->error($model,'cantColumna'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'imagen'); ?>
		<?php echo $form->textArea($model,'imagen',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'imagen'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'AreaAlmacen_id'); ?>
		<?php echo $form->dropDownList($model,'AreaAlmacen_id',Chtml::listData(AreaAlmacen::model()->findAll(),'id','descripcion'), array('null'=>'Ninguno..')); ?>
		<?php echo $form->error($model,'AreaAlmacen_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->