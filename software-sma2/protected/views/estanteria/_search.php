<?php
/* @var $this EstanteriaController */
/* @var $model Estanteria */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'codigo'); ?>
		<?php echo $form->textField($model,'codigo',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'decripcion'); ?>
		<?php echo $form->textField($model,'decripcion',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cantFila'); ?>
		<?php echo $form->textField($model,'cantFila'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cantColumna'); ?>
		<?php echo $form->textField($model,'cantColumna'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'imagen'); ?>
		<?php echo $form->textArea($model,'imagen',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'AreaAlmacen_id'); ?>
		<?php echo $form->textField($model,'AreaAlmacen_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->