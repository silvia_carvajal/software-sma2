<?php
/* @var $this EstanteriaController */
/* @var $data Estanteria */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('codigo')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->codigo), array('view', 'id'=>$data->codigo)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('decripcion')); ?>:</b>
	<?php echo CHtml::encode($data->decripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cantFila')); ?>:</b>
	<?php echo CHtml::encode($data->cantFila); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cantColumna')); ?>:</b>
	<?php echo CHtml::encode($data->cantColumna); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('imagen')); ?>:</b>
	<?php echo CHtml::encode($data->imagen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('AreaAlmacen_id')); ?>:</b>
	<?php echo CHtml::encode($data->AreaAlmacen_id); ?>
	<br />


</div>