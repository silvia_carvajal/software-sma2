<?php
/* @var $this EstanteriaController */
/* @var $model Estanteria */

$this->breadcrumbs=array(
	'Estanterias'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Listar Estanteria', 'url'=>array('index')),
	array('label'=>'Administrar Estanteria', 'url'=>array('admin')),
);
?>

<h1>Create Estanteria</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>