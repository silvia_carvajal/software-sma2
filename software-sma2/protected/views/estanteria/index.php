<?php
/* @var $this EstanteriaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Estanterias',
);

$this->menu=array(
	array('label'=>'Create Estanteria', 'url'=>array('create')),
	array('label'=>'Manage Estanteria', 'url'=>array('admin')),
);
?>

<h1>Estanterias</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
