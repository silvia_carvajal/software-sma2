<?php
/* @var $this EstanteriaController */
/* @var $model Estanteria */

$this->breadcrumbs=array(
	'Estanterias'=>array('index'),
	$model->codigo,
);

$this->menu=array(
	array('label'=>'Listar Estanteria', 'url'=>array('index')),
	array('label'=>'Nuevo Estanteria', 'url'=>array('create')),
	array('label'=>'Modificar Estanteria', 'url'=>array('update', 'id'=>$model->codigo)),
	array('label'=>'Eliminar Estanteria', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->codigo),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Estanteria', 'url'=>array('admin')),
);
?>

<h1>View Estanteria #<?php echo $model->codigo; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'codigo',
		'decripcion',
		'cantFila',
		'cantColumna',
		'imagen',
		'AreaAlmacen_id',
	),
)); ?>
