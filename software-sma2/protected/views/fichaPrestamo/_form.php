<?php
/* @var $this FichaPrestamoController */
/* @var $model FichaPrestamo */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ficha-prestamo-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
		<?php echo $form->error($model,'id'); ?>
	</div>

	       
        
        <div class="row">
		<?php echo $form->labelEx($model,'fechaPrestamo');
                $model->fechaPrestamo=date("Y/m/d");
                echo $form->textField($model,'fechaPrestamo',array('value'=>$model->fechaPrestamo, 'readonly'=>'false','size'=>5,'maxlength'=>10));
                echo $form->error($model,'fechaPrestamo'); 
                ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fechaDevolucion'); ?>
		<?php echo $form->textField($model,'fechaDevolucion'); ?>
		<?php echo $form->error($model,'fechaDevolucion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'observacion'); ?>
		<?php echo $form->textField($model,'observacion',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'observacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'HerramientaEspecial_codigo'); ?>
		<?php echo $form->dropDownList($model,'HerramientaEspecial_codigo',Chtml::listData(HerramientaEspecial::model()->findAll(),'id','descripcion'), array('null'=>'Ninguno..')); ?>
		<?php echo $form->error($model,'HerramientaEspecial_codigo'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'cantidad'); ?>
		<?php echo $form->textField($model,'cantidad'); ?>
		<?php echo $form->error($model,'cantidad'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Tecnico_ci'); ?>
		<?php echo $form->dropDownList($model,'Tecnico_ci',Chtml::listData(Tecnico::model()->findAll(),'ci','nombres','apPat'), array('null'=>'Ninguno..')); ?>
		<?php echo $form->error($model,'Tecnico_ci'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'PersonalOficial_codigo'); ?>
		<?php echo $form->dropDownList($model,'PersonalOficial_codigo',Chtml::listData(PersonalOficial::model()->findAll(),'codigo','ci'), array('null'=>'Ninguno..')); ?>
		<?php echo $form->error($model,'PersonalOficial_codigo'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->