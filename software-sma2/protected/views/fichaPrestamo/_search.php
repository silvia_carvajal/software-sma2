<?php
/* @var $this FichaPrestamoController */
/* @var $model FichaPrestamo */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fechaPrestamo'); ?>
		<?php echo $form->textField($model,'fechaPrestamo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fechaDevolucion'); ?>
		<?php echo $form->textField($model,'fechaDevolucion'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'observacion'); ?>
		<?php echo $form->textField($model,'observacion',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'HerramientaEspecial_codigo'); ?>
		<?php echo $form->textField($model,'HerramientaEspecial_codigo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cantidad'); ?>
		<?php echo $form->textField($model,'cantidad'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Tecnico_ci'); ?>
		<?php echo $form->textField($model,'Tecnico_ci'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'PersonalOficial_codigo'); ?>
		<?php echo $form->textField($model,'PersonalOficial_codigo',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->