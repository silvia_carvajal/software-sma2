<?php
/* @var $this FichaPrestamoController */
/* @var $data FichaPrestamo */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaPrestamo')); ?>:</b>
	<?php echo CHtml::encode($data->fechaPrestamo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaDevolucion')); ?>:</b>
	<?php echo CHtml::encode($data->fechaDevolucion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('observacion')); ?>:</b>
	<?php echo CHtml::encode($data->observacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('HerramientaEspecial_codigo')); ?>:</b>
	<?php echo CHtml::encode($data->HerramientaEspecial_codigo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cantidad')); ?>:</b>
	<?php echo CHtml::encode($data->cantidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Tecnico_ci')); ?>:</b>
	<?php echo CHtml::encode($data->Tecnico_ci); ?>
	<br />

	
	<b><?php echo CHtml::encode($data->getAttributeLabel('PersonalOficial_codigo')); ?>:</b>
	<?php echo CHtml::encode($data->PersonalOficial_codigo); ?>
	<br />

	

</div>