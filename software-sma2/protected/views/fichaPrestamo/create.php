<?php
/* @var $this FichaPrestamoController */
/* @var $model FichaPrestamo */

$this->breadcrumbs=array(
	'Ficha Prestamos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FichaPrestamo', 'url'=>array('index')),
	array('label'=>'Manage FichaPrestamo', 'url'=>array('admin')),
);
?>

<h1>Nuevo FichaPrestamo</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>