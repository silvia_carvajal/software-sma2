<?php
/* @var $this FichaPrestamoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ficha Prestamos',
);

$this->menu=array(
	array('label'=>'Nuevo FichaPrestamo', 'url'=>array('create')),
	array('label'=>'Administrar FichaPrestamo', 'url'=>array('admin')),
);
?>

<h1>Ficha Prestamos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
