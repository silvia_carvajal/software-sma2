<?php
/* @var $this FichaPrestamoController */
/* @var $model FichaPrestamo */

$this->breadcrumbs=array(
	'Ficha Prestamos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Editar',
);

$this->menu=array(
	array('label'=>'Listar FichaPrestamo', 'url'=>array('index')),
	array('label'=>'Nuevo FichaPrestamo', 'url'=>array('create')),
	array('label'=>'Ver FichaPrestamo', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar FichaPrestamo', 'url'=>array('admin')),
);
?>

<h1>Editar FichaPrestamo <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>