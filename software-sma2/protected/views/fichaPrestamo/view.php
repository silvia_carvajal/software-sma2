<?php
/* @var $this FichaPrestamoController */
/* @var $model FichaPrestamo */

$this->breadcrumbs=array(
	'Ficha Prestamos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Listar FichaPrestamo', 'url'=>array('index')),
	array('label'=>'Nuevo FichaPrestamo', 'url'=>array('create')),
	array('label'=>'Editar FichaPrestamo', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Eliminar FichaPrestamo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar FichaPrestamo', 'url'=>array('admin')),
);
?>

<h1>Ver FichaPrestamo #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'fechaPrestamo',
		'fechaDevolucion',
		'observacion',
		'HerramientaEspecial_codigo',
		'cantidad',
		'Tecnico_ci',
		'PersonalOficial_codigo',
	),
)); ?>
