<?php
/* @var $this GradoController */
/* @var $model Grado */

$this->breadcrumbs=array(
	'Grados'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Listar Grado', 'url'=>array('index')),
	array('label'=>'Administrar Grado', 'url'=>array('admin')),
);
?>

<h1>Nuevo Grado</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>