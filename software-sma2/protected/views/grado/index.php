<?php
/* @var $this GradoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Grados',
);

$this->menu=array(
	array('label'=>'Nuevo Grado', 'url'=>array('create')),
	array('label'=>'Administrar Grado', 'url'=>array('admin')),
);
?>

<h1>Grados</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
