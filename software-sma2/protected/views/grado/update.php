<?php
/* @var $this GradoController */
/* @var $model Grado */

$this->breadcrumbs=array(
	'Grados'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Editar',
);

$this->menu=array(
	array('label'=>'Listar Grado', 'url'=>array('index')),
	array('label'=>'Nuevo Grado', 'url'=>array('create')),
	array('label'=>'Ver Grado', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar Grado', 'url'=>array('admin')),
);
?>

<h1>Editar Grado <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>