<?php
/* @var $this HerramienComunController */
/* @var $model HerramienComun */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'herramien-comun-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'codigo'); ?>
		<?php echo $form->textField($model,'codigo'); ?>
		<?php echo $form->error($model,'codigo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
		<?php echo $form->textField($model,'descripcion',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>

	        
        
        <div class="row">
		<?php echo $form->labelEx($model,'fechaIngreso');
                $model->fechaIngreso=date("Y/m/d");
                echo $form->textField($model,'fechaIngreso',array('value'=>$model->fechaIngreso, 'readonly'=>'false','size'=>5,'maxlength'=>10));
                echo $form->error($model,'fechaIngreso'); 
                ?>
	</div>
            
        
        <div class="row">
		<?php echo $form->labelEx($model,'horaIngreso'); 
		$model->horaIngreso=date("h:i:s");
                echo $form->textField($model,'horaIngreso',array('value'=>$model->horaIngreso, 'readonly'=>'false','size'=>5,'maxlength'=>10));
                echo $form->error($model,'horaIngreso'); 
                ?>
	</div>
        
        
        
	<div class="row">
		<?php echo $form->labelEx($model,'marca'); ?>
		<?php echo $form->textField($model,'marca',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'marca'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fila'); ?>
		<?php echo $form->textField($model,'fila',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'fila'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'columna'); ?>
		<?php echo $form->textField($model,'columna',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'columna'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estadoUbicacion'); ?>
		<?php echo $form->textField($model,'estadoUbicacion',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'estadoUbicacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Estanteria_codigo'); ?>
		<?php echo $form->dropDownList($model,'Estanteria_codigo',Chtml::listData(Estanteria::model()->findAll(),'codigo','decripcion'), array('null'=>'Ninguno..')); ?>
		<?php echo $form->error($model,'Estanteria_codigo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TipoItem_id'); ?>
		<?php echo $form->dropDownList($model,'TipoItem_id',Chtml::listData(TipoItem::model()->findAll(),'id','descripcion'), array('null'=>'Ninguno..')); ?>
		<?php echo $form->error($model,'TipoItem_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->