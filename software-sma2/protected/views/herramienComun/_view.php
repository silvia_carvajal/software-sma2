<?php
/* @var $this HerramienComunController */
/* @var $data HerramienComun */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('codigo')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->codigo), array('view', 'id'=>$data->codigo)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaIngreso')); ?>:</b>
	<?php echo CHtml::encode($data->fechaIngreso); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('horaIngreso')); ?>:</b>
	<?php echo CHtml::encode($data->horaIngreso); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('marca')); ?>:</b>
	<?php echo CHtml::encode($data->marca); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fila')); ?>:</b>
	<?php echo CHtml::encode($data->fila); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('columna')); ?>:</b>
	<?php echo CHtml::encode($data->columna); ?>
	<br />

	
	<b><?php echo CHtml::encode($data->getAttributeLabel('estadoUbicacion')); ?>:</b>
	<?php echo CHtml::encode($data->estadoUbicacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Estanteria_codigo')); ?>:</b>
	<?php echo CHtml::encode($data->Estanteria_codigo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TipoItem_id')); ?>:</b>
	<?php echo CHtml::encode($data->TipoItem_id); ?>
	<br />

	

</div>