<?php
/* @var $this HerramienComunController */
/* @var $model HerramienComun */

$this->breadcrumbs=array(
	'Herramien Comuns'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar HerramienComun', 'url'=>array('index')),
	array('label'=>'Nuevo HerramienComun', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#herramien-comun-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Herramien Comuns</h1>

<p>
Opcionalmente, puede introducir un operador de comparación (<, <=,>,> =, <> o =) al principio de cada uno de los valores de búsqueda para especificar cómo se debe hacer la comparación.
</p>


<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'herramien-comun-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'codigo',
		'descripcion',
		'fechaIngreso',
		'horaIngreso',
		'marca',
		'fila',
		
		'columna',
		'estadoUbicacion',
		 'Estanteria_codigo',
		'TipoItem_id',
		
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
