<?php
/* @var $this HerramienComunController */
/* @var $model HerramienComun */

$this->breadcrumbs=array(
	'Herramien Comuns'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Listar HerramienComun', 'url'=>array('index')),
	array('label'=>'Administrar HerramienComun', 'url'=>array('admin')),
);
?>

<h1>Nuevo HerramienComun</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>