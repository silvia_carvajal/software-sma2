<?php
/* @var $this HerramienComunController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Herramien Comuns',
);

$this->menu=array(
	array('label'=>'Nuevo HerramienComun', 'url'=>array('create')),
	array('label'=>'Administrar HerramienComun', 'url'=>array('admin')),
);
?>

<h1>Herramien Comuns</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
