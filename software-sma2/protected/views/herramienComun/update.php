<?php
/* @var $this HerramienComunController */
/* @var $model HerramienComun */

$this->breadcrumbs=array(
	'Herramien Comuns'=>array('index'),
	$model->codigo=>array('view','id'=>$model->codigo),
	'Editar',
);

$this->menu=array(
	array('label'=>'Listar HerramienComun', 'url'=>array('index')),
	array('label'=>'Nuevo HerramienComun', 'url'=>array('create')),
	array('label'=>'Ver HerramienComun', 'url'=>array('view', 'id'=>$model->codigo)),
	array('label'=>'Administrar HerramienComun', 'url'=>array('admin')),
);
?>

<h1>Update HerramienComun <?php echo $model->codigo; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>