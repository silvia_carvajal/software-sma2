<?php
/* @var $this HerramienComunController */
/* @var $model HerramienComun */

$this->breadcrumbs=array(
	'Herramien Comuns'=>array('index'),
	$model->codigo,
);

$this->menu=array(
	array('label'=>'Listar HerramienComun', 'url'=>array('index')),
	array('label'=>'Nuevo HerramienComun', 'url'=>array('create')),
	array('label'=>'Editar HerramienComun', 'url'=>array('update', 'id'=>$model->codigo)),
	array('label'=>'Eliminar HerramienComun', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->codigo),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar HerramienComun', 'url'=>array('admin')),
);
?>

<h1>View HerramienComun #<?php echo $model->codigo; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'codigo',
		'descripcion',
		'fechaIngreso',
		'horaIngreso',
		'marca',
		'fila',
		'columna',
		'estadoUbicacion',
            'Estanteria_codigo',
		'TipoItem_id',
            
	),
)); ?>
