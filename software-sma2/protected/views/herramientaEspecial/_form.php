<?php
/* @var $this HerramientaEspecialController */
/* @var $model HerramientaEspecial */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'herramienta-especial-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
		<?php echo $form->error($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
		<?php echo $form->textField($model,'descripcion',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>

                <?php echo CHtml::activeFileField($model,'image'); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nroCertificado'); ?>
		<?php echo $form->textField($model,'nroCertificado',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'nroCertificado'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nroFactura'); ?>
		<?php echo $form->textField($model,'nroFactura',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'nroFactura'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nroParte'); ?>
		<?php echo $form->textField($model,'nroParte',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'nroParte'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nroSerie'); ?>
		<?php echo $form->textField($model,'nroSerie',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'nroSerie'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fechaCalibracion'); ?>
		<?php echo $form->textField($model,'fechaCalibracion'); ?>
		<?php echo $form->error($model,'fechaCalibracion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'eligilidad'); ?>
		<?php echo $form->textField($model,'eligilidad',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'eligilidad'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'detalleOrganizacion'); ?>
		<?php echo $form->textField($model,'detalleOrganizacion',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'detalleOrganizacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'autoridadAprovacion'); ?>
		<?php echo $form->textField($model,'autoridadAprovacion',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'autoridadAprovacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fechaEntrega'); ?>
		<?php echo $form->textField($model,'fechaEntrega'); ?>
		<?php echo $form->error($model,'fechaEntrega'); ?>
	</div>

	      
        
        <div class="row">
		<?php echo $form->labelEx($model,'fechaIngreso');
                $model->fechaIngreso=date("Y/m/d");
                echo $form->textField($model,'fechaIngreso',array('value'=>$model->fechaIngreso, 'readonly'=>'false','size'=>5,'maxlength'=>10));
                echo $form->error($model,'fechaIngreso'); 
                ?>
	</div>
        

	<div class="row">
		<?php echo $form->labelEx($model,'contactoOrganizacon'); ?>
		<?php echo $form->textField($model,'contactoOrganizacon',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'contactoOrganizacon'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fabricante'); ?>
		<?php echo $form->textField($model,'fabricante',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'fabricante'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'agenciaCertificacion'); ?>
		<?php echo $form->textField($model,'agenciaCertificacion',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'agenciaCertificacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'condicion'); ?>
		<?php echo $form->textField($model,'condicion',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'condicion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'commingInspection'); ?>
		<?php echo $form->textField($model,'commingInspection',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'commingInspection'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fila'); ?>
		<?php echo $form->textField($model,'fila',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'fila'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'columna'); ?>
		<?php echo $form->textField($model,'columna',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'columna'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estadoUbicacion'); ?>
		<?php echo $form->textField($model,'estadoUbicacion',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'estadoUbicacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estanteria_codigo'); ?>
		<?php echo $form->dropDownList($model,'estanteria_codigo',Chtml::listData(Estanteria::model()->findAll(),'codigo','descripcion'), array('null'=>'Ninguno..')); ?>
		<?php echo $form->error($model,'estanteria_codigo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estado_id'); ?>
		<?php echo $form->dropDownList($model,'estado_id',Chtml::listData(Estado::model()->findAll(),'id','nombreEstado'), array('null'=>'Ninguno..')); ?>
		<?php echo $form->error($model,'estado_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TipoItem_id'); ?>
		<?php echo $form->dropDownList($model,'TipoItem_id',Chtml::listData(TipoItem::model()->findAll(),'id','descripcion'), array('null'=>'Ninguno..')); ?>
		<?php echo $form->error($model,'TipoItem_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->