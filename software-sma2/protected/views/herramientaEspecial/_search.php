<?php
/* @var $this HerramientaEspecialController */
/* @var $model HerramientaEspecial */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'descripcion'); ?>
		<?php echo $form->textField($model,'descripcion',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'imagen'); ?>
		<?php echo $form->textArea($model,'imagen',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nroCertificado'); ?>
		<?php echo $form->textField($model,'nroCertificado',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nroFactura'); ?>
		<?php echo $form->textField($model,'nroFactura',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nroParte'); ?>
		<?php echo $form->textField($model,'nroParte',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nroSerie'); ?>
		<?php echo $form->textField($model,'nroSerie',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fechaCalibracion'); ?>
		<?php echo $form->textField($model,'fechaCalibracion'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'eligilidad'); ?>
		<?php echo $form->textField($model,'eligilidad',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'detalleOrganizacion'); ?>
		<?php echo $form->textField($model,'detalleOrganizacion',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'autoridadAprovacion'); ?>
		<?php echo $form->textField($model,'autoridadAprovacion',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fechaEntrega'); ?>
		<?php echo $form->textField($model,'fechaEntrega'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fechaIngreso'); ?>
		<?php echo $form->textField($model,'fechaIngreso'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contactoOrganizacon'); ?>
		<?php echo $form->textField($model,'contactoOrganizacon',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fabricante'); ?>
		<?php echo $form->textField($model,'fabricante',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'agenciaCertificacion'); ?>
		<?php echo $form->textField($model,'agenciaCertificacion',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'condicion'); ?>
		<?php echo $form->textField($model,'condicion',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'commingInspection'); ?>
		<?php echo $form->textField($model,'commingInspection',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fila'); ?>
		<?php echo $form->textField($model,'fila',array('size'=>5,'maxlength'=>5)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'columna'); ?>
		<?php echo $form->textField($model,'columna',array('size'=>5,'maxlength'=>5)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'estadoUbicacion'); ?>
		<?php echo $form->textField($model,'estadoUbicacion',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'estanteria_codigo'); ?>
		<?php echo $form->textField($model,'estanteria_codigo',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'estado_id'); ?>
		<?php echo $form->textField($model,'estado_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TipoItem_id'); ?>
		<?php echo $form->textField($model,'TipoItem_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->