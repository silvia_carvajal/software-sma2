<?php
/* @var $this HerramientaEspecialController */
/* @var $model HerramientaEspecial */

$this->breadcrumbs=array(
	'Herramienta Especials'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar HerramientaEspecial', 'url'=>array('index')),
	array('label'=>'Nuevo HerramientaEspecial', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#herramienta-especial-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Herramienta Especials</h1>

<p>
Opcionalmente, puede introducir un operador de comparación (<, <=,>,> =, <> o =) al principio de cada uno de los valores de búsqueda para especificar cómo se debe hacer la comparación.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'herramienta-especial-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'descripcion',
		'imagen',
		'nroCertificado',
		'nroFactura',
		'nroParte',
		
		'nroSerie',
		'fechaCalibracion',
		'eligilidad',
		'detalleOrganizacion',
		'autoridadAprovacion',
		'fechaEntrega',
		'fechaIngreso',
		'contactoOrganizacon',
		'fabricante',
		'agenciaCertificacion',
		'condicion',
		'commingInspection',
		'fila',
		'columna',
		'estadoUbicacion',
		'estanteria_codigo',
		'estado_id',
		'TipoItem_id',
		
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
