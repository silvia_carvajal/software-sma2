<?php
/* @var $this HerramientaEspecialController */
/* @var $model HerramientaEspecial */

$this->breadcrumbs=array(
	'Herramienta Especials'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Listar HerramientaEspecial', 'url'=>array('index')),
	array('label'=>'Administrar HerramientaEspecial', 'url'=>array('admin')),
);
?>

<h1>Nuevo HerramientaEspecial</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>