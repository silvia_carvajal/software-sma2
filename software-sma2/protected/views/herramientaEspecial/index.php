<?php
/* @var $this HerramientaEspecialController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Herramienta Especials',
);

$this->menu=array(
	array('label'=>'Nuevo HerramientaEspecial', 'url'=>array('create')),
	array('label'=>'Administrar HerramientaEspecial', 'url'=>array('admin')),
);
?>

<h1>Herramienta Especials</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
