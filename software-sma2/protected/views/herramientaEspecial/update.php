<?php
/* @var $this HerramientaEspecialController */
/* @var $model HerramientaEspecial */

$this->breadcrumbs=array(
	'Herramienta Especials'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Editar',
);

$this->menu=array(
	array('label'=>'Listar HerramientaEspecial', 'url'=>array('index')),
	array('label'=>'Nuevo HerramientaEspecial', 'url'=>array('create')),
	array('label'=>'Ver HerramientaEspecial', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar HerramientaEspecial', 'url'=>array('admin')),
);
?>

<h1>Editar HerramientaEspecial <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>