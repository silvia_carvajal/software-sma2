<?php
/* @var $this HerramientaEspecialController */
/* @var $model HerramientaEspecial */

$this->breadcrumbs=array(
	'Herramienta Especials'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Listar HerramientaEspecial', 'url'=>array('index')),
	array('label'=>'Nuevo HerramientaEspecial', 'url'=>array('create')),
	array('label'=>'Editar HerramientaEspecial', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Eliminar HerramientaEspecial', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar HerramientaEspecial', 'url'=>array('admin')),
);
?>

<h1>Ver HerramientaEspecial #<?php echo $model->id; ?></h1>
<?php echo CHtml::image(Yii::app()->baseUrl."/images/".$model->imagen); ?>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'descripcion',
		'nroCertificado',
		'nroFactura',
		'nroParte',
		'nroSerie',
		'fechaCalibracion',
		'eligilidad',
		'detalleOrganizacion',
		'autoridadAprovacion',
		'fechaEntrega',
		'fechaIngreso',
		'contactoOrganizacon',
		'fabricante',
		'agenciaCertificacion',
		'condicion',
		'commingInspection',
		'fila',
		'columna',
		'estadoUbicacion',
		'estanteria_codigo',
		'estado_id',
		'TipoItem_id',
	),
)); ?>
