<?php
/* @var $this PersonalOficialController */
/* @var $model PersonalOficial */

$this->breadcrumbs=array(
	'Personal Oficials'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar PersonalOficial', 'url'=>array('index')),
	array('label'=>'Nuevo PersonalOficial', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#personal-oficial-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Personal Oficials</h1>

<p>
Opcionalmente, puede introducir un operador de comparación (<, <=,>,> =, <> o =) al principio de cada uno de los valores de búsqueda para especificar cómo se debe hacer la comparación.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'personal-oficial-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'codigo',
		'ci',
		'nombres',
		'apPat',
		'apMat',
		'sexo',
		'fechaNac',
		'direccion',
		'tipoSangre',
		'Cargo_id',
		'Grado_id',
		
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
