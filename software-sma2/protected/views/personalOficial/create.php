<?php
/* @var $this PersonalOficialController */
/* @var $model PersonalOficial */

$this->breadcrumbs=array(
	'Personal Oficials'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Listar PersonalOficial', 'url'=>array('index')),
	array('label'=>'Administrar PersonalOficial', 'url'=>array('admin')),
);
?>

<h1>Create PersonalOficial</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>