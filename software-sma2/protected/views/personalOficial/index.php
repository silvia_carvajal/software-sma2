<?php
/* @var $this PersonalOficialController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Personal Oficials',
);

$this->menu=array(
	array('label'=>'Nuevo PersonalOficial', 'url'=>array('create')),
	array('label'=>'Administrar PersonalOficial', 'url'=>array('admin')),
);
?>

<h1>Personal Oficials</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
