<?php
/* @var $this PersonalOficialController */
/* @var $model PersonalOficial */

$this->breadcrumbs=array(
	'Personal Oficials'=>array('index'),
	$model->codigo=>array('view','id'=>$model->codigo),
	'Editar',
);

$this->menu=array(
	array('label'=>'List PersonalOficial', 'url'=>array('index')),
	array('label'=>'Create PersonalOficial', 'url'=>array('create')),
	array('label'=>'View PersonalOficial', 'url'=>array('view', 'id'=>$model->codigo)),
	array('label'=>'Manage PersonalOficial', 'url'=>array('admin')),
);
?>

<h1>Update PersonalOficial <?php echo $model->codigo; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>