<?php
/* @var $this PersonalOficialController */
/* @var $model PersonalOficial */

$this->breadcrumbs=array(
	'Personal Oficials'=>array('index'),
	$model->codigo,
);

$this->menu=array(
	array('label'=>'Listar PersonalOficial', 'url'=>array('index')),
	array('label'=>'Nuevo PersonalOficial', 'url'=>array('create')),
	array('label'=>'Editar PersonalOficial', 'url'=>array('update', 'id'=>$model->codigo)),
	array('label'=>'Eliminar PersonalOficial', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->codigo),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar PersonalOficial', 'url'=>array('admin')),
);
?>

<h1>View PersonalOficial #<?php echo $model->codigo; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'codigo',
		'ci',
		'nombres',
		'apPat',
		'apMat',
		'sexo',
		'fechaNac',
		'direccion',
		'tipoSangre',
		'Cargo_id',
		'Grado_id',
	),
)); ?>
