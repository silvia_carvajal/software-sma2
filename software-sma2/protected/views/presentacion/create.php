<?php
/* @var $this PresentacionController */
/* @var $model Presentacion */

$this->breadcrumbs=array(
	'Presentacions'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Listar Presentacion', 'url'=>array('index')),
	array('label'=>'Administrar Presentacion', 'url'=>array('admin')),
);
?>

<h1>Nuevo Presentacion</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>