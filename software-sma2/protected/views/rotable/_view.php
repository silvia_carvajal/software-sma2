<?php
/* @var $this RotableController */
/* @var $data Rotable */
?>

<div class="view">
    
         <?php echo CHtml::image(Yii::app()->baseUrl."/images/".$data->imagen); ?>

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaIngreso')); ?>:</b>
	<?php echo CHtml::encode($data->fechaIngreso); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nroFactura')); ?>:</b>
	<?php echo CHtml::encode($data->nroFactura); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nroCertifcado')); ?>:</b>
	<?php echo CHtml::encode($data->nroCertifcado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nroParte')); ?>:</b>
	<?php echo CHtml::encode($data->nroParte); ?>
	<br />

	
	<b><?php echo CHtml::encode($data->getAttributeLabel('nroSerie')); ?>:</b>
	<?php echo CHtml::encode($data->nroSerie); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('condicion')); ?>:</b>
	<?php echo CHtml::encode($data->condicion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fabricante')); ?>:</b>
	<?php echo CHtml::encode($data->fabricante); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaEntrega')); ?>:</b>
	<?php echo CHtml::encode($data->fechaEntrega); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('eligibilidad')); ?>:</b>
	<?php echo CHtml::encode($data->eligibilidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('detalleOrganizacion')); ?>:</b>
	<?php echo CHtml::encode($data->detalleOrganizacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contactoOrganizacion')); ?>:</b>
	<?php echo CHtml::encode($data->contactoOrganizacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fila')); ?>:</b>
	<?php echo CHtml::encode($data->fila); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('columna')); ?>:</b>
	<?php echo CHtml::encode($data->columna); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estadoUbicacion')); ?>:</b>
	<?php echo CHtml::encode($data->estadoUbicacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Estanteria_codigo')); ?>:</b>
	<?php echo CHtml::encode($data->Estanteria_codigo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado_id')); ?>:</b>
	<?php echo CHtml::encode($data->estado_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TipoAvion_id')); ?>:</b>
	<?php echo CHtml::encode($data->TipoAvion_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TipoItem_id')); ?>:</b>
	<?php echo CHtml::encode($data->TipoItem_id); ?>
	<br />



</div>