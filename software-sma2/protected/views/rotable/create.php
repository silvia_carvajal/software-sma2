<?php
/* @var $this RotableController */
/* @var $model Rotable */

$this->breadcrumbs=array(
	'Rotables'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Listar Rotable', 'url'=>array('index')),
	array('label'=>'Administrar Rotable', 'url'=>array('admin')),
);
?>

<h1>Nuevo Rotable</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>