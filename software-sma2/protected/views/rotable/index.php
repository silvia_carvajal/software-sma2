<?php
/* @var $this RotableController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Rotables',
);

$this->menu=array(
	array('label'=>'Nuevo Rotable', 'url'=>array('create')),
	array('label'=>'Administrar Rotable', 'url'=>array('admin')),
);
?>

<h1>Rotables</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
