<?php
/* @var $this RotableController */
/* @var $model Rotable */

$this->breadcrumbs=array(
	'Rotables'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Editar',
);

$this->menu=array(
	array('label'=>'Listar Rotable', 'url'=>array('index')),
	array('label'=>'Editar Rotable', 'url'=>array('create')),
	array('label'=>'Ver Rotable', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar Rotable', 'url'=>array('admin')),
);
?>

<h1>Update Rotable <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>