<?php
/* @var $this RotableController */
/* @var $model Rotable */

$this->breadcrumbs=array(
	'Rotables'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Listar Rotable', 'url'=>array('index')),
	array('label'=>'Nuevo Rotable', 'url'=>array('create')),
	array('label'=>'Editar Rotable', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Eliminar Rotable', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Rotable', 'url'=>array('admin')),
);
?>

<h1>Vista Rotable #<?php echo $model->id; ?></h1>
<?php echo CHtml::image(Yii::app()->baseUrl."/images/".$model->imagen); ?>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'descripcion',
		'fechaIngreso',
		'nroFactura',
		'nroCertifcado',
		'nroParte',
		'nroSerie',
		'condicion',
		'fabricante',
		'fechaEntrega',
		'eligibilidad',
		'detalleOrganizacion',
		'contactoOrganizacion',
		'fila',
		'columna',
		'estadoUbicacion',
		'Estanteria_codigo',
		'estado_id',
		'TipoAvion_id',
		'TipoItem_id',
	),
)); ?>
