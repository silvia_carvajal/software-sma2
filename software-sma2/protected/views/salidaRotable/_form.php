<?php
/* @var $this SalidaRotableController */
/* @var $model SalidaRotable */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'salida-rotable-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
		<?php echo $form->error($model,'id'); ?>
	</div>
       
        
        <div class="row">
		<?php echo $form->labelEx($model,'fechaSalida');
                $model->fechaSalida=date("Y/m/d");
                echo $form->textField($model,'fechaSalida',array('value'=>$model->fechaSalida, 'readonly'=>'false','size'=>5,'maxlength'=>10));
                echo $form->error($model,'fechaSalida'); 
                ?>
	</div>
        
        

	<div class="row">
		<?php echo $form->labelEx($model,'Rotable_id'); ?>
		<?php echo $form->dropDownList($model,'Rotable_id',Chtml::listData(Rotable::model()->findAll(),'id','descripcion'), array('null'=>'Ninguno..')); ?>
		<?php echo $form->error($model,'Rotable_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Tecnico_id'); ?>
		<?php echo $form->dropDownList($model,'Tecnico_id',Chtml::listData(Tecnico::model()->findAll(),'ci','nombres','apPat'), array('null'=>'Ninguno..')); ?>
		<?php echo $form->error($model,'Tecnico_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'PersonalOficial_codigo'); ?>
		<?php echo $form->dropDownList($model,'PersonalOficial_codigo',Chtml::listData(PersonalOficial::model()->findAll(),'codigo','ci','nombres'), array('null'=>'Ninguno..')); ?>
		<?php echo $form->error($model,'PersonalOficial_codigo'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->