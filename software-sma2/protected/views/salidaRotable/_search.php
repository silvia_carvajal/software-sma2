<?php
/* @var $this SalidaRotableController */
/* @var $model SalidaRotable */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fechaSalida'); ?>
		<?php echo $form->textField($model,'fechaSalida'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Rotable_id'); ?>
		<?php echo $form->textField($model,'Rotable_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Tecnico_id'); ?>
		<?php echo $form->textField($model,'Tecnico_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'PersonalOficial_codigo'); ?>
		<?php echo $form->textField($model,'PersonalOficial_codigo',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->