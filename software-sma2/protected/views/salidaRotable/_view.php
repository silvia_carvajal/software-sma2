<?php
/* @var $this SalidaRotableController */
/* @var $data SalidaRotable */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaSalida')); ?>:</b>
	<?php echo CHtml::encode($data->fechaSalida); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Rotable_id')); ?>:</b>
	<?php echo CHtml::encode($data->Rotable_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Tecnico_id')); ?>:</b>
	<?php echo CHtml::encode($data->Tecnico_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PersonalOficial_codigo')); ?>:</b>
	<?php echo CHtml::encode($data->PersonalOficial_codigo); ?>
	<br />


</div>