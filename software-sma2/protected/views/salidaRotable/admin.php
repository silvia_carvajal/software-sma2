<?php
/* @var $this SalidaRotableController */
/* @var $model SalidaRotable */

$this->breadcrumbs=array(
	'Salida Rotables'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Listar SalidaRotable', 'url'=>array('index')),
	array('label'=>'Nuevo SalidaRotable', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#salida-rotable-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Salida Rotables</h1>

<p>
Opcionalmente, puede introducir un operador de comparación (<, <=,>,> =, <> o =) al principio de cada uno de los valores de búsqueda para especificar cómo se debe hacer la comparación.
</p>


<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'salida-rotable-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'fechaSalida',
		'Rotable_id',
		'Tecnico_id',
		'PersonalOficial_codigo',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
