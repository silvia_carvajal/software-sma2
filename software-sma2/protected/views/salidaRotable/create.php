<?php
/* @var $this SalidaRotableController */
/* @var $model SalidaRotable */

$this->breadcrumbs=array(
	'Salida Rotables'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Listar SalidaRotable', 'url'=>array('index')),
	array('label'=>'Administrar SalidaRotable', 'url'=>array('admin')),
);
?>

<h1>Nuevo SalidaRotable</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>