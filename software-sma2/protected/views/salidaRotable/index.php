<?php
/* @var $this SalidaRotableController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Salida Rotables',
);

$this->menu=array(
	array('label'=>'Nuevo SalidaRotable', 'url'=>array('create')),
	array('label'=>'Administrar SalidaRotable', 'url'=>array('admin')),
);
?>

<h1>Salida Rotables</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
