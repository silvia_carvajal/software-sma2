<?php
/* @var $this SalidaRotableController */
/* @var $model SalidaRotable */

$this->breadcrumbs=array(
	'Salida Rotables'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Editar',
);

$this->menu=array(
	array('label'=>'Listar SalidaRotable', 'url'=>array('index')),
	array('label'=>'Nuevo SalidaRotable', 'url'=>array('create')),
	array('label'=>'Ver SalidaRotable', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar SalidaRotable', 'url'=>array('admin')),
);
?>

<h1>Editar SalidaRotable <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>