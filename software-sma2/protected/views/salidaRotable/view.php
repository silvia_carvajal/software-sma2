<?php
/* @var $this SalidaRotableController */
/* @var $model SalidaRotable */

$this->breadcrumbs=array(
	'Salida Rotables'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Listar SalidaRotable', 'url'=>array('index')),
	array('label'=>'Nuevo SalidaRotable', 'url'=>array('create')),
	array('label'=>'Editar SalidaRotable', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Eliminar SalidaRotable', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar SalidaRotable', 'url'=>array('admin')),
);
?>

<h1>Ver SalidaRotable #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'fechaSalida',
		'Rotable_id',
		'Tecnico_id',
		'PersonalOficial_codigo',
	),
)); ?>
