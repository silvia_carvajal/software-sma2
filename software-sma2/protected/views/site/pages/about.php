<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - About';
?>


<center><h1>SERVICIO DE MANTENIMIETO AÉREO  No. 2 CBBA</h1></center>
    <div class="AN1">
            <p>El Servicio de Mantenimiento Aéreo ¨2¨ es un Taller Certificado por la DGAC 
                como una Organización de Mantenimiento Aprobado OMA 145/1/007/14, en la actualidad
                se constituye en el pilar fundamental de la Fuerza Aérea Boliviana para realizar el 
                mantenimiento de aeronaves militares y civiles, acorde a los estándares y 
                reglamentaciones emitidas por Autoridad Aeronáutica Civil y Militar, de esta 
                manera estamos contribuyendo con el engrandecimiento de la Aeronáutica en su conjunto,
                brindando a los Operadores Aéreos Servicios con Seguridad y Calidad..</p>
       
            <center>
            <img src="images/puertaprincipal.jpg" alt="Edificio del S.M.A. No. 2"/>
            </center>
            <center><p><i>Instalaciones de F.A.B IIBrigada Aerea S.M.A.2</i></p></br></center>
            <ul>
                <li><h4><b>Efeméride</b></h4></li>
            </ul>
            <p>En fecha 22 de febrero de 1996 se creo el servicio de Mantenimiento
                Aereo Nº "2"; fijándose como base de esta unidad técnica el hangar Nº2 
                en la II Brigada Aérea de Cochabamba. Dependerá en lo administrativo del 
                comando general a traves del departamento</p>
            <ul>
                <li><h4><b>Creación</h4></b></li>
            </ul>
            <p>En fecha 28 de Mayo de 1998 por resolución del Comando General de
                la F.N.B. No. 18/98, se cambia el nombre de AMA II por Área Naval
                No. 2 Santa Cruz de la Sierra. El Comando de Unidad está ubicado 
                en el barrio del Pajonal de la ciudad de Santa Cruz de la Sierra.
            </p>
            <center>
            <img src="images/escudosma.jpg" alt="escudo del S.M.A. No. 2"/>
            </center>
            
            <center><p><i>Escudo del S.M.A. No. 2</i></p></br></center>
            
            <ul>
                <li><h4><b>Unidades Dependientes</b></h4>
                    <ul>
                        <li>F.A.B Brigada Aérea II</li>
             
                    </ul>
                    <center>
            <img src="images/mision2.jpg" alt="Almacen del S.M.A. No. 2"/>
            </center>
            
            <center><p><i>Escudo del S.M.A. No. 2</i></p></br></center>
                </li>
            </ul>
            <ul>
                <li><h4><b>Reseña Histórica</b></h4></li>
            </ul>
            <p>En fecha 05-ENE-1975 mediante orden general de la F.N.B. se dispone 
                la creación de la oficina de enlace de la F.N.B. con asiento en 
                la ciudad de Santa Cruz de la Sierra.</p>
            <p>En fecha 24 de marzo de 1989, por disposición del Comando General 
                de la Armada Boliviana, la oficina de enlace cambia de nombre por
                área de Mantenimiento y Abastecimiento II (AMA-II) y pasa a 
                depender directamente del Dpto. IV logístico.</p>
            <p>Al hacerse cargo el Cap. Corb. DEMN. Hugo Roca Morales de la Unidad 
                en elaño 1995, viendo la necesidad del crecimiento vegetativo del 
                personal tanto de Cuadros como de Tropas, le da otra dinámica y 
                da inicio a la implementación y construcción de numerosas dependencias; 
                es así que se construyen oficinas para la Plana Mayor, cancha deportiva, 
                Palco de Honor, tanque de Agua con su respectivo pozo, seguridad para
                la Unidad (amurallado del perímetro, prevención de guardia), como
                así también nuevas dependencias para la tropa (Dormitorios, furrielatos,
                Sanidad Naval) incluyendo los servicios básicos (cocina, panadería, 
                comedor, baños, duchas, snack), también se construyen instalaciones
                para la Div. IV Log. Consistentes en depósitos para las diferentes 
                clases incluyendo una Estación de Servicio para Motorizados.</p>
            <p>Actualmente se está finalizando la remodelación y ampliación del
                Comando y se ha dado inicio a la construcción del Casino, Jardines 
                y Áreas de esparcimiento.</p>
            <ul>
            
        </div>
    
