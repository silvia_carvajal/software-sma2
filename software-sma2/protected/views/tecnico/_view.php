<?php
/* @var $this TecnicoController */
/* @var $data Tecnico */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ci')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ci), array('view', 'id'=>$data->ci)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombres')); ?>:</b>
	<?php echo CHtml::encode($data->nombres); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('apPat')); ?>:</b>
	<?php echo CHtml::encode($data->apPat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('apMat')); ?>:</b>
	<?php echo CHtml::encode($data->apMat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sexo')); ?>:</b>
	<?php echo CHtml::encode($data->sexo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaNac')); ?>:</b>
	<?php echo CHtml::encode($data->fechaNac); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('direccion')); ?>:</b>
	<?php echo CHtml::encode($data->direccion); ?>
	<br />

	
	<b><?php echo CHtml::encode($data->getAttributeLabel('codigo')); ?>:</b>
	<?php echo CHtml::encode($data->codigo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Grado_id')); ?>:</b>
	<?php echo CHtml::encode($data->Grado_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Unidad_codigo')); ?>:</b>
	<?php echo CHtml::encode($data->Unidad_codigo); ?>
	<br />



</div>