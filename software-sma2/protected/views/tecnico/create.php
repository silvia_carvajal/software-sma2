<?php
/* @var $this TecnicoController */
/* @var $model Tecnico */

$this->breadcrumbs=array(
	'Tecnicos'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Listar Tecnico', 'url'=>array('index')),
	array('label'=>'Administrar Tecnico', 'url'=>array('admin')),
);
?>

<h1>Create Tecnico</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>