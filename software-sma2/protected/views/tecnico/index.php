<?php
/* @var $this TecnicoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tecnicos',
);

$this->menu=array(
	array('label'=>'Nuevo Tecnico', 'url'=>array('create')),
	array('label'=>'Administrar Tecnico', 'url'=>array('admin')),
);
?>

<h1>Tecnicos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
