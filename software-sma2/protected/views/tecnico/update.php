<?php
/* @var $this TecnicoController */
/* @var $model Tecnico */

$this->breadcrumbs=array(
	'Tecnicos'=>array('index'),
	$model->ci=>array('view','id'=>$model->ci),
	'Update',
);

$this->menu=array(
	array('label'=>'Listar Tecnico', 'url'=>array('index')),
	array('label'=>'Nuevo Tecnico', 'url'=>array('create')),
	array('label'=>'ver Tecnico', 'url'=>array('view', 'id'=>$model->ci)),
	array('label'=>'Administrar Tecnico', 'url'=>array('admin')),
);
?>

<h1>Update Tecnico <?php echo $model->ci; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>