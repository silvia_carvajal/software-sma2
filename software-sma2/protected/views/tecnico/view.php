<?php
/* @var $this TecnicoController */
/* @var $model Tecnico */

$this->breadcrumbs=array(
	'Tecnicos'=>array('index'),
	$model->ci,
);

$this->menu=array(
	array('label'=>'Listar Tecnico', 'url'=>array('index')),
	array('label'=>'Nuevo Tecnico', 'url'=>array('create')),
	array('label'=>'Editar Tecnico', 'url'=>array('update', 'id'=>$model->ci)),
	array('label'=>'Eliminar Tecnico', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ci),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Tecnico', 'url'=>array('admin')),
);
?>

<h1>View Tecnico #<?php echo $model->ci; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ci',
		'nombres',
		'apPat',
		'apMat',
		'sexo',
		'fechaNac',
		'direccion',
		'codigo',
		'Grado_id',
		'Unidad_codigo',
	),
)); ?>
