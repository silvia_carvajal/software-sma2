<?php
/* @var $this TipoAvionController */
/* @var $data TipoAvion */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('decripcion')); ?>:</b>
	<?php echo CHtml::encode($data->decripcion); ?>
	<br />


</div>