<?php
/* @var $this TipoAvionController */
/* @var $model TipoAvion */

$this->breadcrumbs=array(
	'Tipo Avions'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Listar TipoAvion', 'url'=>array('index')),
	array('label'=>'Administrar TipoAvion', 'url'=>array('admin')),
);
?>

<h1>Nuevo TipoAvion</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>