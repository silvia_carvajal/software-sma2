<?php
/* @var $this TipoAvionController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tipo Avions',
);

$this->menu=array(
	array('label'=>'Nuevo TipoAvion', 'url'=>array('create')),
	array('label'=>'Administrar TipoAvion', 'url'=>array('admin')),
);
?>

<h1>Tipo Avions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
