<?php
/* @var $this TipoAvionController */
/* @var $model TipoAvion */

$this->breadcrumbs=array(
	'Tipo Avions'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Editar',
);

$this->menu=array(
	array('label'=>'Listar TipoAvion', 'url'=>array('index')),
	array('label'=>'Nuevo TipoAvion', 'url'=>array('create')),
	array('label'=>'Ver TipoAvion', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar TipoAvion', 'url'=>array('admin')),
);
?>

<h1>Editar TipoAvion <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>