<?php
/* @var $this TipoAvionController */
/* @var $model TipoAvion */

$this->breadcrumbs=array(
	'Tipo Avions'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Listar TipoAvion', 'url'=>array('index')),
	array('label'=>'Nuevo TipoAvion', 'url'=>array('create')),
	array('label'=>'Editar TipoAvion', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Eliminar TipoAvion', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar TipoAvion', 'url'=>array('admin')),
);
?>

<h1>Ver TipoAvion #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'decripcion',
	),
)); ?>
