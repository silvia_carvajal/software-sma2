<?php
/* @var $this TipoContactoController */
/* @var $model TipoContacto */

$this->breadcrumbs=array(
	'Tipo Contactos'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Listar TipoContacto', 'url'=>array('index')),
	array('label'=>'Administrar TipoContacto', 'url'=>array('admin')),
);
?>

<h1>Nuevo TipoContacto</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>