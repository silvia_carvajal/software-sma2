<?php
/* @var $this TipoContactoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tipo Contactos',
);

$this->menu=array(
	array('label'=>'Nuevo TipoContacto', 'url'=>array('create')),
	array('label'=>'Administrar TipoContacto', 'url'=>array('admin')),
);
?>

<h1>Tipo Contactos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
