<?php
/* @var $this TipoContactoController */
/* @var $model TipoContacto */

$this->breadcrumbs=array(
	'Tipo Contactos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Editar',
);

$this->menu=array(
	array('label'=>'Listar TipoContacto', 'url'=>array('index')),
	array('label'=>'Nuevo TipoContacto', 'url'=>array('create')),
	array('label'=>'Ver TipoContacto', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar TipoContacto', 'url'=>array('admin')),
);
?>

<h1>Editar TipoContacto <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>