<?php
/* @var $this TipoContactoController */
/* @var $model TipoContacto */

$this->breadcrumbs=array(
	'Tipo Contactos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Listar TipoContacto', 'url'=>array('index')),
	array('label'=>'Nuevo TipoContacto', 'url'=>array('create')),
	array('label'=>'Editar TipoContacto', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Eliminar TipoContacto', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar TipoContacto', 'url'=>array('admin')),
);
?>

<h1>Ver TipoContacto #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'descripcion',
	),
)); ?>
