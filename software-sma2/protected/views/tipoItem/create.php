<?php
/* @var $this TipoItemController */
/* @var $model TipoItem */

$this->breadcrumbs=array(
	'Tipo Items'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Listar TipoItem', 'url'=>array('index')),
	array('label'=>'Administrar TipoItem', 'url'=>array('admin')),
);
?>

<h1>Nuevo TipoItem</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>