<?php
/* @var $this TipoItemController */
/* @var $model TipoItem */

$this->breadcrumbs=array(
	'Tipo Items'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Editar',
);

$this->menu=array(
	array('label'=>'Listar TipoItem', 'url'=>array('index')),
	array('label'=>'Nuevo TipoItem', 'url'=>array('create')),
	array('label'=>'Ver TipoItem', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar TipoItem', 'url'=>array('admin')),
);
?>

<h1>Editar TipoItem <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>