<?php
/* @var $this TipoItemController */
/* @var $model TipoItem */

$this->breadcrumbs=array(
	'Tipo Items'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Listar TipoItem', 'url'=>array('index')),
	array('label'=>'Nuevo TipoItem', 'url'=>array('create')),
	array('label'=>'Editar TipoItem', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Eliminar TipoItem', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar TipoItem', 'url'=>array('admin')),
);
?>

<h1>Ver TipoItem #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'descripcion',
		'cantidad',
	),
)); ?>
