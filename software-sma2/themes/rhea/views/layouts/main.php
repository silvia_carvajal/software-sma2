<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
            <div id="escudo">
            <img src="images/escudos.png" alt="Heraldica" style="width: 100%; height: 100%">
            </div>
            <div id="logo">
                <img src="images/faro2.png" alt="faro" style="width: 15%; height: 18%">
                <?php echo CHtml::encode(Yii::app()->name); ?>
            </div>
                
	</div><!-- header -->

    <div id="mainmenu">    
	<div id="topbar" >
		<?php $this->widget('zii.widgets.CMenu',array(
                     'items'=>array(
			array('label'=>'Inicio', 'url'=>array('/site/index')),
			array('label'=>'S.M.A. 2', 'url'=>array('/site/page', 'view'=>'about')),
			array('label'=>'Contáctenos', 'url'=>array('/site/contact')),
                        array('label'=>'Iniciar Sesión', 'url'=>Yii::app()->user->ui->loginUrl, 'visible'=>Yii::app()->user->isGuest),
			array('label'=>'Cerrar Sesión ('.Yii::app()->user->name.')', 'url'=>Yii::app()->user->ui->logoutUrl, 'visible'=>!Yii::app()->user->isGuest),				
                       
                         
                        
                        array('label'=>'Prestamo ', 'url'=>array('/site/index'),
                                'items'=>array(
                                array('label'=>'Ficha prestamo', 'url'=>array('/fichaPrestamo/index')),
                                array('label'=>'Registrar Prestamo', 'url'=>array('/fichaPrestamo/index')), 
                                    ),'visible'=>!Yii::app()->user->isGuest),
                        
                        array('label'=>'Almacen ', 'url'=>array('/site/index'),
                                'items'=>array(
                                     array('label'=>'Gestionar Herramienta comun', 'url'=>array('/herramienComun/index')),
                                     array('label'=>'Administrar Herramienta Especial', 'url'=>array('/herramientaEspecial/index')),
                                     array('label'=>'Administrar Rotable', 'url'=>array('/rotable/index')),
                                    array('label'=>'Administrar Consumible', 'url'=>array('/consumible/index')),
                                     array('label'=>'Gestionar Estanteria', 'url'=>array('/estanteria/index')),
                                    array('label'=>'Gestionar Area Almacen', 'url'=>array('/areaAlmacen/index')),
                                    array('label'=>'Registrar Tipo Avion', 'url'=>array('/tipoAvion/index')),
                                     array('label'=>'Gestionar Unidad Medida', 'url'=>array('/unidadMedida/index')),
                                     array('label'=>'Gestionar Estado', 'url'=>array('/estado/index')),
                                     array('label'=>'Gestionar Presentación', 'url'=>array('/presentacion/index')),
                                     ),'visible'=>!Yii::app()->user->isGuest),
                        
                       array('label'=>'salida  ', 'url'=>array('/site/index'),
                                'items'=>array(
                                     array('label'=>'Registrar Salida de Rotable', 'url'=>array('/salidaRotable/index')),
                                     array('label'=>'Registrar Detalle Salida', 'url'=>array('/salidaRotable/index')),
                                     
                                     ),'visible'=>!Yii::app()->user->isGuest),
                        array('label'=>'Personas', 'url'=>array('/site/vPersona'),
                            'items'=>array(
                                array('label'=>'Cuenta de Usuarios','url'=>Yii::app()->user->ui->userManagementAdminUrl, 'visible'=>!Yii::app()->user->isGuest),
                                array('label'=>'Gestionar Usuario', 'url'=>array('/usuario/index')),
                                array('label'=>'Gestionar Cliente', 'url'=>array('/cliente/index')),
                                
                                array('label'=>'Gestionar Tecnico', 'url'=>array('/tecnico/index')),  
                                array('label'=>'Registrar Unidad', 'url'=>array('/unidad/index')), 
                                array('label'=>'Gestionar Personal de Almacen', 'url'=>array('/personalOficial/index')), 
                                array('label'=>'Resgistrar Cargo', 'url'=>array('/cargo/index')), 
                                array('label'=>'Registrar Grado', 'url'=>array('/grado/index')),
                                array('label'=>'Registrar Contacto Tecnico', 'url'=>array('/contactoTecnico/index')),
                                array('label'=>'Registrar tipos de contacto', 'url'=>array('/tipoContacto/index')),
                            ),'visible'=>!Yii::app()->user->isGuest),
                        
                        
                    ),
		)); ?>
	</div><!-- mainmenu -->
    </div>    
      
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
			'homeLink'=>''
		)); ?><!-- breadcrumbs -->
	<?php endif?>
                
        <!-- VER MENSAJES FLASH -->
        <div class="info" style="text-align: left;">
        <?php
            $flashMessages = Yii::app()->user->getFlashes();
            if($flashMessages){
                echo '<ul class="flashes">';
                foreach($flashMessages as $key=>$message){
                    echo '<li><div class="flash-'.$key.'">'.$message."</div></li>\n";
                }
                echo '</ul>';
            }
        ?>
        </div>        

	<div id="main-content">
		<?php echo $content; ?>
	</div>
	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> por L.Fernando Colque y L. Enrique Mamani.<br/>
		Todos los derechos reservados.<br/>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>

<?php
//efecto para el div de Mensaje Flash
Yii::app()->clientScript->registerScript(
   'myHideEffect',
   '$(".info").animate({opacity: 1.0},10000).slideUp("slow");',
   CClientScript::POS_READY    
);
?>
